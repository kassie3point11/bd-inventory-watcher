FROM python:3.10-alpine

ENV SEEN_IDS_PATH=/data/seen_ids.json

RUN pip install discord-webhook==0.16.3
CMD python3 -m bd_inventory_watcher
COPY bd_inventory_watcher/ /usr/local/lib/python3.10/site-packages/bd_inventory_watcher
