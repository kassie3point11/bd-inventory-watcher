import decimal
from enum import Enum
from types import NoneType
from typing import Union, Dict

import requests
import json


class BadDragonToySize(Enum):
    OneSize = 6
    Mini = 10
    Small = 1
    Medium = 2
    Large = 8
    XLarge = 3
    XXLarge = 287
    PawSingle = 269


class BadDragonToyFirmness(Enum):
    XSoft = 5
    Soft = 7
    Medium = 4
    Firm = 9
    Split1 = 121
    Split2 = 12
    Split3 = 123
    Split4 = 11
    Split5 = 124


class BadDragonToy:
    def __init__(self, json_toy, toy_skus: Union[Dict, NoneType] = None):
        if toy_skus is None or len(toy_skus) == 0:
            toy_skus = get_product_list()

        from decimal import Decimal, getcontext
        getcontext().prec = 2
        self.id: int = json_toy["id"]
        self.sku: str = json_toy["sku"]
        self.friendly_name: str = toy_skus[self.sku]
        self.price: Decimal = Decimal(json_toy["price"])
        self.size: str = BadDragonToySize(json_toy["size"]).name
        self.firmness: str = BadDragonToyFirmness(json_toy["firmness"]).name
        self.cumtube: bool = (int(json_toy["cumtube"]) == 13)
        self.suction_cup: bool = (int(json_toy["suction_cup"]) == 23)
        self.weight: Decimal = Decimal(json_toy["weight"])
        self.is_flop: bool = json_toy["is_flop"]
        self.flop_reason: str = json_toy["external_flop_reason"]
        self.color_display: str = json_toy["color_display"]
        self.original_price: Decimal = Decimal(json_toy["original_price"])
        self.image: str = json_toy["images"][0]["imageUrlFull"]

    def __str__(self):
        toy_str = f"**{self.friendly_name}** "
        if self.is_flop:
            toy_str += f"[Flop - {self.flop_reason}]"
        if self.cumtube > 0:
            toy_str += f"[Cumtube]"
        if self.suction_cup:
            toy_str += f"[Suction Cup]"
        toy_str += f"[{self.size}/{self.firmness}] "
        if self.price != self.original_price:
            toy_str += f"~~${self.original_price}~~ "
        toy_str += f"${self.price} "
        toy_str += f"<{self.image}>"
        return toy_str


api_base_url = "https://bad-dragon.com/api"


def get_toy_page(page: int):
    request_url = api_base_url + f"/inventory-toys?type[]=ready_made&type[]=flop&price[min]=0&price[max]=300&" \
                                 f"sort[field]=price&&sort[direction]=asc&limit=60&page={page}"
    response = requests.get(request_url)
    return response.json()


def get_all_toys(toy_skus: Union[NoneType, Dict] = None):
    if toy_skus is None or len(toy_skus) == 0:
        toy_skus = get_product_list()

    toys = []
    last_api_resp = {"limit": 0, "page": 0, "toys": []}
    while len(last_api_resp["toys"]) == last_api_resp["limit"]:
        last_api_resp = get_toy_page(last_api_resp["page"] + 1)
        last_api_resp["limit"] = int(last_api_resp["limit"])
        last_api_resp["page"] = int(last_api_resp["page"])
        for toy in last_api_resp["toys"]:
            toys.append(BadDragonToy(toy, toy_skus))
    return toys


def get_product_list():
    request_url = api_base_url + "/inventory-toys/product-list"
    response = requests.get(request_url)
    toy_sku_list = response.json()
    return {toy['sku']: toy['name'] for toy in toy_sku_list}
