import json
import os
from time import sleep
from . import bad_dragon_api
from discord_webhook import DiscordWebhook

print(f"loading seen toys from {os.path.abspath(os.environ['SEEN_IDS_PATH'])}")
seen_ids = []
if os.path.exists(os.environ["SEEN_IDS_PATH"]):
    with open(os.environ["SEEN_IDS_PATH"]) as f:
        seen_ids = json.load(f)

print("getting toys")
toys = bad_dragon_api.get_all_toys()

wh_url = os.environ["WEBHOOK_URL"]
wh_str = ""
new_toys = [toy for toy in toys if not (toy.id in seen_ids)]
print(f"got {len(toys)} toys (new: {len(new_toys)}), sending on discord")
for toy in new_toys:
    seen_ids.append(toy.id)
    if len(wh_str) + len(str(toy)) > 2000:
        webhook = DiscordWebhook(url=wh_url, content=wh_str)
        webhook.execute()
        wh_str = ""
        print("sent message, sleeping for 5s")
        sleep(5)
    wh_str += str(toy) + "\n"

if len(wh_str.strip("\r\n\t ")) != 0:
    webhook = DiscordWebhook(url=wh_url, content=wh_str)
    webhook.execute()

with open(os.environ["SEEN_IDS_PATH"], "w") as f:
    json.dump(seen_ids, f)
